## Grokking Patterns  
  
#### Sliding Windows :   
 * To be used in scenarios to find or calculate something among all the contiguous subarrays (or sublists) of a given size   
 * When finding maximum or minimum values, initialize variable with `Integer.MIN_VALUE` or `Integer.MAX_VALUE` respectively. Then use `Math.max()` or `Math.min`  
  
##### Golden Rules  
 * Initialize the starting index to 0
 * Find out what would be the length of resultant array and initialize it
	 * Generally, it would be n - (k - 1)
	 * See explanation [here](https://bitbucket.org/kodepad/grokking/src/master/1_slidingWindow/src/main/java/sw/sw1_ContiguousSubArrayAverage.java)
	 * If using brute force, outer loop will iterate with this

 * Initialize for loop with `end Index,` traversing to entire array length
 	* As f(x) has to include all elements even if its the last block of kth element
 * Sliding window reuses f(x), hence it has to be calculate inside for loop

 * In case of a fixed size window
	 * Add a if condition, which would be breaking point / exit condition for the first time
	 * Generally it would be, if `end Index >= k -1` , (kth index)

 * In case of a variable length sized window
	 * Use a while loop, as it will make sure to shrink the window to a variable size
	 * Generally it would be, `while(f(x) >= k)` 
	 
 * Last two conditions in the if block would be 
 * Update/ remove the starting element from f(x), for f(x) to be reused
 * Increment starting index

  
## References  
  
https://github.com/anyulu/grokking-the-coding-interview


> Written with [StackEdit](https://stackedit.io/).