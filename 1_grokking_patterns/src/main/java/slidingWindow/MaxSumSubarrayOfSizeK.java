package slidingWindow;

public class MaxSumSubarrayOfSizeK {

  /* Given an array of positive numbers and a positive number ‘k,’ find the maximum sum of any contiguous subarray of size ‘k’.
   */

  public static int findMaxSumSubarray(int[] arr, int k) {

    int sum = 0;
    int startIndex = 0;
    int maxSum = Integer.MIN_VALUE;

    for (int endIndex = 0; endIndex < arr.length; endIndex++) {
      sum += arr[endIndex];

      if (endIndex >= k - 1) {
        maxSum = Math.max(sum, maxSum);
        sum -= arr[startIndex];
        startIndex++;
      }
    }
    return maxSum;
  }

  public static void main(String[] args) {
    int[] arr1 = {1, 2, 3, 4, 5, 6};
    int[] arr2 = {2, 3, 4, 1, 5};
    int[] arr3 = {2, 1, 5, 1, 3, 2};

    int k1 = 3;
    int k2 = 2;
    int k3 = 3;

    System.out.println(findMaxSumSubarray(arr1, k1));
    System.out.println(findMaxSumSubarray(arr2, k2));
    System.out.println(findMaxSumSubarray(arr3, k3));
  }
}
