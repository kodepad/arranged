package slidingWindow;

import java.util.Arrays;

public class ContiguousSubArraySum {

  /*
  Given an array, find the average of all contiguous subarrays of size ‘K’ in it.
   */

  public static double[] findAverageOfSubarray(int[] arr, int k) {

    double[] result = new double[arr.length - k + 1];
    int startIndex = 0;
    double sum = 0;

    for (int endIndex = 0; endIndex < arr.length; endIndex++) {
      sum = sum + arr[endIndex];

      if (endIndex >= k - 1) {
        result[startIndex] = sum / k;
        sum = sum - arr[startIndex];
        startIndex++;
      }
    }
    return result;
  }

    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 5};
        int k = 2;

        double[] result = findAverageOfSubarray(arr, k);

        System.out.println("Input");
        System.out.println("k : " + k + " arr : " + Arrays.toString(arr));
        System.out.println("Output");
        System.out.println(Arrays.toString(result));
    }
}
