package slidingWindow;

/* Question
Given an array of positive numbers and a positive number ‘S,’ find the length of the smallest contiguous subarray whose sum is greater than or equal to ‘S’. Return 0 if no such subarray exists.
 */
public class SmallestSubarrayWithGivenSum {

    private static int smallestSubArray(int[] arr, int k) {
        int sum = 0;
        int startIndex = 0;
        int subArrayLength = 0;
        int minLength = Integer.MAX_VALUE;


        for(int endIndex =0; endIndex < arr.length; endIndex++)  // for loop -> incrementing the window
        {
            sum += arr[endIndex];
            while(sum>=k)
            {
                subArrayLength = endIndex - startIndex + 1;
                minLength = Math.min(minLength, subArrayLength);
                sum -=  arr[startIndex];
                startIndex++;
            }
        }
        return minLength;
    }


    public static void main(String[] args) {
        int[] arr1 = {2, 1, 5, 2, 3, 2};
        int[] arr2 = {2, 1, 5, 2, 8};
        int[] arr3 = {3, 4, 1, 1, 6};

        int k1 = 7;
        int k2 = 7;
        int k3 = 8;

        int result1 = smallestSubArray(arr1, k1);
        System.out.println((result1));

        int result2 = smallestSubArray(arr2, k2);
        System.out.println((result2));

        int result3 = smallestSubArray(arr3, k3);
        System.out.println((result3));
    }
}
