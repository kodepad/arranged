package slidingWindow;

/* Leetcode 643
 Question :
 Given an array consisting of n integers, find the contiguous subarray of given length k that has the maximum average value. And you need to output the maximum average value.
  */


// TODO : Same soltuion while using float, fails at test cases in LeetCode, check their precision

public class MaximumAvgSubArray1 {

    private static double findMaxAvgSubarray(int[] arr, int k) {
        int startIndex = 0;
        double maxAvg = -Double.MAX_VALUE;
        double sum = 0;

        for(int endIndex = 0; endIndex < arr.length ; endIndex++)
        {
            sum = sum + arr[endIndex];

            if(endIndex >= k-1){
                maxAvg = Math.max(maxAvg, sum/k);
                sum -= sum - arr[startIndex];
                startIndex++;
            }
        }
        return maxAvg;
    }


    public static void main(String[] args) {

        int[] arr = {1,12,-5,-6,50,3};
        int k = 4;
        System.out.println(findMaxAvgSubarray(arr, k));
    }
}
